; Auto-generated. Do not edit!


(cl:in-package meturone_egitim-msg)


;//! \htmlinclude dummy.msg.html

(cl:defclass <dummy> (roslisp-msg-protocol:ros-message)
  ((n
    :reader n
    :initarg :n
    :type cl:integer
    :initform 0))
)

(cl:defclass dummy (<dummy>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <dummy>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'dummy)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name meturone_egitim-msg:<dummy> is deprecated: use meturone_egitim-msg:dummy instead.")))

(cl:ensure-generic-function 'n-val :lambda-list '(m))
(cl:defmethod n-val ((m <dummy>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader meturone_egitim-msg:n-val is deprecated.  Use meturone_egitim-msg:n instead.")
  (n m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <dummy>) ostream)
  "Serializes a message object of type '<dummy>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'n)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'n)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'n)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'n)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <dummy>) istream)
  "Deserializes a message object of type '<dummy>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'n)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'n)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'n)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'n)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<dummy>)))
  "Returns string type for a message object of type '<dummy>"
  "meturone_egitim/dummy")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'dummy)))
  "Returns string type for a message object of type 'dummy"
  "meturone_egitim/dummy")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<dummy>)))
  "Returns md5sum for a message object of type '<dummy>"
  "2582759d796cd9137ed1d713264952a6")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'dummy)))
  "Returns md5sum for a message object of type 'dummy"
  "2582759d796cd9137ed1d713264952a6")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<dummy>)))
  "Returns full string definition for message of type '<dummy>"
  (cl:format cl:nil "uint32 n~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'dummy)))
  "Returns full string definition for message of type 'dummy"
  (cl:format cl:nil "uint32 n~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <dummy>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <dummy>))
  "Converts a ROS message object to a list"
  (cl:list 'dummy
    (cl:cons ':n (n msg))
))
