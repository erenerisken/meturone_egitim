;; Auto-generated. Do not edit!


(when (boundp 'meturone_egitim::dummy)
  (if (not (find-package "METURONE_EGITIM"))
    (make-package "METURONE_EGITIM"))
  (shadow 'dummy (find-package "METURONE_EGITIM")))
(unless (find-package "METURONE_EGITIM::DUMMY")
  (make-package "METURONE_EGITIM::DUMMY"))

(in-package "ROS")
;;//! \htmlinclude dummy.msg.html


(defclass meturone_egitim::dummy
  :super ros::object
  :slots (_n ))

(defmethod meturone_egitim::dummy
  (:init
   (&key
    ((:n __n) 0)
    )
   (send-super :init)
   (setq _n (round __n))
   self)
  (:n
   (&optional __n)
   (if __n (setq _n __n)) _n)
  (:serialization-length
   ()
   (+
    ;; uint32 _n
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint32 _n
       (write-long _n s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint32 _n
     (setq _n (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get meturone_egitim::dummy :md5sum-) "2582759d796cd9137ed1d713264952a6")
(setf (get meturone_egitim::dummy :datatype-) "meturone_egitim/dummy")
(setf (get meturone_egitim::dummy :definition-)
      "uint32 n
")



(provide :meturone_egitim/dummy "2582759d796cd9137ed1d713264952a6")


